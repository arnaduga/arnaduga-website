---
title: "HTTPS sur Gitalb Pages et nom de domain personnalisé"
date: 2021-09-18T12:38:25+02:00
draft: false
summary: "Comment paramétrer un certificat pour exposer votre Gitlab Pages en HTTPS sur votre nom de domaine personnalisé"
tags: ["francais","gitlab"]
categories: ["devops"]
translation: "set-https-gitlab-custom-domain"
---


# Comment paramétrer un nom de domain personnalisé sur Gitlab Pages

> ... en incluant un certificat Let's Encrypt pour l'exposer en HTTPS !


## Le choix de l'hébergement

Quand j'ai décidé de construire mon propre blogue pour mes notes, mes considérations et mes investigations techniques, j'ai dû bien évidemment choisir mon principe d'hébergement.

Je voulais faire un site statique, facile à mettre à jour, à moindre coût et exposé en `https`. En plus de tout cela, je voulais pouvoir avoir une chaine de déployment continu CICD... le tout avec un nom de domaine personnalisé.

Voici donc le résultat d'évaluations faites de quelques solutions.

 | Solution                           | Avantages                                                                                                                                                                                                | Inconvénients                                                                                                                                                                 |
 | ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
 | Hébergement web classique          | Service géré. Nom de domaine personnalisé.                                                                                                                                                               | La chaine CICD doit scripter du `ftp`. Le process pour l'obtention d'un certificat peut être compliqué ou onéreux. Solution globalement chère.                                |
 | Raspberry Pi                       | Autonomie > 100%. Pas de coûts récurrents (excepté un peu d'électricité)                                                                                                                                 | De nombreuses tâches de maintenance (mise à jour OS). Une chaine de déploiment pas simple à construire. Onéreux en matériel (équivalent à plusieurs années de nom de domaine) |
 | S3 avec l'option site web statique | Très peu cher (quelques euros, voire moins, par an). Nom de domaine personnalisable                                                                                                                      | _Rien à déclarer_ :smile:                                                                                                                                                     |
 | Github Pages                       | Solution que j'ai utilisé plus intensément ces deux dernières années (version _Entreprise_). Nom de domaine personnalisable (à confirmer)                                                                | Pas d'outil de CICD, d'où la nécessité d'utiliser Jenkins ou Travis.                                                                                                          |
 | Gitlab Pages                       | Déjà utilisé pour des sujets perso. Inclut l'outil de CICD Gitlab-CI (et quelques heures de _build_ gratuitement). Nom de domaine personnalisable. Possibilité de paramétrer un certificat Let's Encrypt | _Rien à déclarer_                                                                                                                                                             |


En partant de cette liste, mon choix s'est porté sur **Gitlab Pages**, et **Gitlab CI** comme l'outil d'intégration continue.

## Gitlab Pages

Pour plus de détails sur l'utilisation de Gitlab Pages, vous devriez lire la [documentation](https://docs.gitlab.com/ee/user/project/pages/).

Avant d'entrer dans le paramétrage des pages et du nom de domaine personnalisé, j'ai dû préparer un site statique de base, de façon à ce que Gitlab CI ait quelque chose à se mettre sous la dent. :innocent:

J'ai utilisé une version vierge de site généré par l'outil [HUGO](https://gohugo.io).

Ensuite, j'ai ajouté le fichier de base de Gitlab-CI pour un site Hugo, nommé `.gitlab-ci.yaml` : 

```yaml
image: registry.gitlab.com/pages/hugo:latest

variables:
GIT_SUBMODULE_STRATEGY: recursive

test:
script:
    - hugo
except:
    variables:
    - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
script:
    - hugo
artifacts:
    paths:
    - public
only:
    variables:
    - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

Rien de magique ici, ni même de spécial. En fait, il s'agit d'un simple copier/coller du [fichier modèle](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Pages/Hugo.gitlab-ci.yml) proposé par Gitlab !

En tout cas, le résultat est plaisant : à chaque `push` sur ma branche `main`, cela "compile" mon site Hugo !

Si tout s'est bien passé, vous devriez maintenant avoir votre site Gitlab Pages disponible. Le mien se trouvait à l'adresse [`https://arnaduga.gitlab.io/arnaduga-website/`](https://arnaduga.gitlab.io/arnaduga-website/), c'est pour ça que je voulais un nom de domaine personnalisé.

Maintenant que les bases sont posées, parlons du nom de domaine personnalisé et de son certificat.

### Étape 1 : Enregistrer un nom de domaine

Pour ma part, j'utilise [Gandi](https://gandi.net) pour gérer mes noms de domaines.

J'ai sélectionné le nom `arnaduga.dev`, mais il y a une contrainte à son utilisation :

![images](../set-https-gitlab-custom-domain/gandi-dev-tld-constraint.png)

... c'est une des raisons qui m'ont poussé à vouloir un HTTPS :smile:

### Étape 2 : Créer un nouveau domaine dans les paramètres Gitlab Pages

Dans votre dépôt Gitlab, aller dans `Settings` > `Pages` > bouton `New Domain`.

Dans le formulaire proposé, renseignez le nom de domaine que vous venez d'acheter et assurez vous que l'option **_Automatic certificate management using Let's Encrypt_** est bien sélectionnée.

![image](../set-https-gitlab-custom-domain/website-gitlab-custom-domain.png)


### Étape 3 : Mettre à jour les enregistrements DNS

Dans votre outil de gestion de DNS, éditez les enregistrements, en en ajoutant **DEUX**, tels que mentionnés sur la page de configuration Gitlab (après avoir cliqué sur le bouton _**Create New Domain**_) :

| From                                           | DNS Record | To                                                                |
| ---------------------------------------------- | ---------- | ----------------------------------------------------------------- |
| `arnaduga.dev`                                 | A          | `35.185.44.232`                                                   |
| `_gitlab-pages-verification-code.arnaduga.dev` | TXT        | `gitlab-pages-verification-code=bda9f41fd4ff3bef595190d769025366` |



> **IMPORTANT**
> 
> La façon dont ces enregistrements sont affichés dans la page ou la doc m'a menée à une mauvaise interprétation et donc, des erreurs.
>
> Voici mes configurations en place :
>  
> ![image](../set-https-gitlab-custom-domain/gandi-setup.png)
> 
> Comme vous pouvez le constater, ces deux enregistrements sont nommés `@` et le terme `_gitlab-pages-verification-code.arnaduga.dev` n'est même pas utilisé !


Une fois que c'est fait, vous devriez pouvoir faire vérifier que vous êtes bien le propriétaire du domaine :

![image](../set-https-gitlab-custom-domain/website-gitlab-dns-validation.png)

Après quelques minutes, votre certificat **Let's Encrypt** sera généré et paramétré !

:tada: **Et voilà !** :tada:

> **Note**
> 
> Il est fort possible que certains enregistrements DNS par défaut entrent en conflict avec cette configuration. Je me souviens en avoir supprimé quelques uns (un ou deux, peut-être), mais malheureusement, je ne sais plus lesquels. Désolé.
> 
> Si vous créer un nouveau nom de domaine che Gandi, pourriez-vous me lister les enregistrements par défaut ? Cela me permettrait de corriger cet article en le complétant. Merci ! :wink:

## Notes à propos des "secrets"

Comme vous avez vu dans cet article, je partage quelques informations DNS (comme mon code de vérification Gitlab par exemple). 

Mais rassurez-vous, ces informations ne sont PAS des secrets, étant donné qu'elles sont partagées avec les DNS !

Pour vous en convaincre, sur Mac ou LInux, essayez ces commandes `dig` : 

```
$ dig arnaduga.dev TXT

; <<>> DiG 9.10.6 <<>> arnaduga.dev TXT
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 17169
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;arnaduga.dev.			IN	TXT

;; ANSWER SECTION:
arnaduga.dev.		10800	IN	TXT	"v=spf1 include:_mailcust.gandi.net ?all"
arnaduga.dev.		10800	IN	TXT	"gitlab-pages-verification-code=bda9f41fd4ff3bef595190d769025366"

;; Query time: 64 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Wed Sep 15 18:08:21 CEST 2021
;; MSG SIZE  rcvd: 169
```

```
$ dig arnaduga.dev A

; <<>> DiG 9.10.6 <<>> arnaduga.dev A
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3607
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;arnaduga.dev.			IN	A

;; ANSWER SECTION:
arnaduga.dev.		10800	IN	A	35.185.44.232

;; Query time: 53 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Wed Sep 15 18:08:25 CEST 2021
;; MSG SIZE  rcvd: 57
```

Alors, vous vouyez ? Ce ne sont pas des secrets ! :smile: