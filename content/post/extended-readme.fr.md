---
title: "Lisez-moi étendu"
date: 2021-08-31T00:00:00+02:00
draft: false
pin: false
summary: "À propos de moi et de ce site, quelques informations complémentaires."
tags: ["francais"]
translation: "extended-readme"
---

# À propos

Oui, je sais, vous allez vous demander pourquoi je démarre un site maintenant, de type blog, en 2021.

Je peux vous entendre d'ici : *"Tu devrais utiliser Twitter, Facebook, Medium ou d'autres médias !"*.

Oui. Je pourrais. Mais j'ai aussi voulu tester quelque chose d'autre, quelque chose de nouveau (pour moi) : [HUGO](https://gohugo.io) !

Avant d'aller plus loin sur ce sujet, commençons par les présentations.


## Qui suis-je ?

Je suis un architecte solution et API, 40 ans et quelques, qui travaille dans l'informatique depuis le *XXème siècle* :satisfied: !

Je travaille sur plusieurs types de sujets : transformation d'applications vers le cloud, accompagnement d'équipes DevOps, aide aux équipes de développement pour la définiton de l'architecture logicielle (pas les interfaces graphiques : même si j'ai quelques idées, ce n'est pas ma tasse de thé), définition des contrats d'interface API, _évangélisation_ API, etc.

Je suis très curieux de nature et ouvert d'esprit (notamment quand cela touche les nouvelles techno et l'informatique) : j'adore découvrir de nouveaux sujets, de nouveaux outils, de nouveaux principes, de nouveaux concepts (même si je dois avouer que je suis un peu perdu dans le monde des _blockchains_ et des cryptomonnaies).


> **Note**
> 
> Je ne suis **PAS** développeur. 
> 
> Je "code" pour des projets perso, pour Arduino ou Raspberry, ou un peu en NodeJs pour développer des API, avec un peu de Docker, etc... Mais je ne développe pas pour mon travail (à part un peu d'_infra-as-code_, comme du Terraform/Ansible) : il y a plein de bons développeurs qui font ça bien mieux que moi !
>
> Toutefois, j'aime le code. J'aime travailler avec des équipes de développement !


## Pourquoi ce blog?

Disons que j'avais envie d'écrire et partager quelque notes, idées et découvertes. Peut-être même en faire mon pense-bête.

Mon compte [Twitter](https://twitter/com/@arnaduga) n'est pas vraiment approprié pour cela, trop restreint (_and Yes, Ranjit, I can be chatty, I know!_)... Il ne m'en a pas fallu plus pour me lancer.


## Pourquoi des articles en _English_ 🇬🇧, si je suis français 🇫🇷?

Je sais que je fais énormément de fautes en anglais (un peu moins en français, j'espère :confounded: ). Mais en fait, j'ai toujours travaillé pour de grandes sociétés, à l'échelle mondiale, avec des équipes délocalisées, souvent en Inde 🇮🇳, mais aussi Thaïlande 🇹🇭 et Roumanie 🇷🇴). 

Alors, il m'a semblé naturel d'utiliser cette lange pour des sujets techniques. Qui plus est, la littérature IT est majoritairement anglaise.

Et puis, soit dit en passant ... il n'y a pas toujours de vocabulaire adapté en français dans ce domaine : vous me voyez parler d'_informatique nuagique_ pour du _cloud computing_ :joy: ? Ça pourrait être amusant, mais ça compliquerait la lecture (et l'écriture). :smile:


Néanmoins, ne vous inquiétez pas, il y aura des articles en français (exclusifs ou traduits).


## Informations à propos de ce site

Ce site est donc construit en utilisant [HUGO](https://gohugo.io/) et [le thème github-style forké et adapté à ma sauce](https://github.com/arnaduga/github-style).

Il est hébergé sur [Gitlab pages](https://gitlab.com).

Les dates du contenus pourront être trafiquées (antidatées, par exemple) : j'ai de nombreuses notes et une multitude de tests que je pourrais partager ici, que j'ai commencé il y a longtemps. J'essaierai donc de les placer correctement sur la ligne du temps.


## Liens partagés

Les liens mentionnés sur ce site (vers Amazon, Gandi, Ali, ou autres) **NE SONT PAS** sponsorisés. 

Je ne gagnerai donc rien si vous les utilisez :smile: !