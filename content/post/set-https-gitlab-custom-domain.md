---
title: "Set Https Gitlab Custom Domain"
date: 2021-09-11T15:32:20+02:00
draft: false
summary: "Quick post on how to set the Gitlab Pages Custom Domain with DNS ownership validation"
pin: false
tags: ["gitlab","english"]
categories: ['DevOps']
translation: "set-https-gitlab-custom-domain.fr"
---

# How to setup a custom domain name on Gitlab Pages

> ... including the Let's Encrypt certificate!

## Hosting choice

When I decided to build a new blog for my technical notes, considerations and investigations, I went to decide _where_ hosting it. 

I wanted the website static, easy to update, at a minimum cost and exposed on `https`. In addition to that, having a CICD pipeline to deploy a new version was also in my requirements.... and a custom domain name.

Here are some evaluations I've quickly done.

| Solution              | Pros                                                                                                                   | Cons                                                                                                                        |
| --------------------- | ---------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| Classical web hosting | Managed service. Custom domain                                                                                         | CICD Pipeline uses `ftp`. Certificate process could be an issue. Expensive solution                                         |
| Personal Raspberry Pi | > 100% autonomous. No recurrent expenses (except small of electricity)                                                 | A lot of maintenance (OS). Deployment pipeline not easy. Expensive in hardware (equivalent of several years of domain name) |
| S3 web static option  | Quite cheap (few cents a year). Customizable domain name                                                               | _Nothing in particular_ :smile:                                                                                             |
| Github Pages          | More heavily used since 2 years (enterprise version). Custom domain name (to be confirmed)                             | No integrated CICD, needs to use Travis or equivalent                                                                       |
| Gitlab Pages          | Already used for personal projects. GitlabCI included. Custom domain name possible. Let's Encrypt certificate possible | _Nothing to mention_                                                                                                        |

Based on this, I decided to go with **Gitlab Pages**, its **GitlabCI** CICD tool.


## Gitlab Pages

For further details on Gitlab Pages, you should read [the documentation](https://docs.gitlab.com/ee/user/project/pages/).

Before setting all the pages and the custom domain, I had to prepare a first basic version of my website (to let Gitlabs Pages have something to crunch).

I used a basic blank [HUGO](https://gohugo.io) website.

Then, I added as well a very basic `.gitlab-ci.yaml` file:

```yaml
image: registry.gitlab.com/pages/hugo:latest

variables:
GIT_SUBMODULE_STRATEGY: recursive

test:
script:
    - hugo
except:
    variables:
    - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
script:
    - hugo
artifacts:
    paths:
    - public
only:
    variables:
    - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

No magic here, or even nothing special as it is a copy/paste from the [template file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Pages/Hugo.gitlab-ci.yml) provided by Gitlab! 

However, it complies my Hugo sources on every push on `main` branch.

If everything is going well, you should now have your Gitlab Pages available. Mine was on address [`https://arnaduga.gitlab.io/arnaduga-website/`](https://arnaduga.gitlab.io/arnaduga-website/).

Now, the basis are there, let's discuss about the custom domain name and its certificate.

### Step1: Domain name registration

On my side, I use [Gandi](https://gandi.net) for my domain names.

I selected the domain `arnaduga.dev`, but it comes with a constraint: 

![image](gandi-dev-tld-constraint.png)

> **Particular conditions**
> 
> Domain names `.dev` have been added to the Google list "HSTS Preload", that implies every internet website in .dev to be configured in HTTPS.
> To make your website works correctly in browsers, you have to get an SSL certificate and to configure the HTTS service.

... that one of the reason why I wanted to have my website in HTTPS :smile:

### Step 2: Create a new domain on your Gitlab Pages setup

On your Gitlab repo, move to `Settings` > `Pages` > button `New Domain`.

Then, on the form, enter your purchased domain name and ensure the option **Automatic certificate management using Let's Encrypt** is enabled.

![image](website-gitlab-custom-domain.png)


### Step 3: Update your DNS records

On your DNS entry management tool, edit the DNS records, by adding 2 records:

| From                                           | DNS Record | To                                                                |
| ---------------------------------------------- | ---------- | ----------------------------------------------------------------- |
| `arnaduga.dev`                                 | A          | `35.185.44.232`                                                   |
| `_gitlab-pages-verification-code.arnaduga.dev` | TXT        | `gitlab-pages-verification-code=bda9f41fd4ff3bef595190d769025366` |


> **IMPORTANT**
> 
> The way these entries are described (copy/paste for the doc) lead me to a wrong setup on Gandi
> Here is MY setup in Gandi DNS record
> ![image](gandi-setup.png)
>
> As you can see, the 2 added records are named `@` and the `_gitlab-pages-verification-code.arnaduga.dev` is even not used!


Once this is done, you should be able to verified your custom domain name:

![image](website-gitlab-dns-validation.png)


After few minutes, your **Let's Encrypt** certificate is generated and setup!

:tada: **Et voilà !** :tada:

> **Note**
> 
> There are maybe some default DNS records that will be in conflict with the whole process. I remembered I deleted some, but, unfortunately, I forgot which ones. My bads.
> May you be able to identify them, I will be happy to update the post (via a PR :wink: )


## Notes on "secrets"

As you saw in this post, I shared some DNS setup and validation keys from Gitlab, etc.

But _do not worry_, these are NOT secrets at all, as there are publicly shared with DNS!

As a proof, on a Mac or Linux, try this:
```
$ dig arnaduga.dev TXT

; <<>> DiG 9.10.6 <<>> arnaduga.dev TXT
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 17169
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;arnaduga.dev.			IN	TXT

;; ANSWER SECTION:
arnaduga.dev.		10800	IN	TXT	"v=spf1 include:_mailcust.gandi.net ?all"
arnaduga.dev.		10800	IN	TXT	"gitlab-pages-verification-code=bda9f41fd4ff3bef595190d769025366"

;; Query time: 64 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Wed Sep 15 18:08:21 CEST 2021
;; MSG SIZE  rcvd: 169
```

```
$ dig arnaduga.dev A

; <<>> DiG 9.10.6 <<>> arnaduga.dev A
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3607
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;arnaduga.dev.			IN	A

;; ANSWER SECTION:
arnaduga.dev.		10800	IN	A	35.185.44.232

;; Query time: 53 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Wed Sep 15 18:08:25 CEST 2021
;; MSG SIZE  rcvd: 57
```

You see? Finally, it is not _that_ secret :smile: