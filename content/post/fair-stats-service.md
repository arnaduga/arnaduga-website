---
title: "Fair Statistics Service"
date: 2021-10-15T18:16:45+02:00
summary: "Web Analytics statistics without using Google Analytics: GoatCounter!"
draft: false
pin: false
tags: ["english","tools"]
translation: "fair-stats-service.fr"
---

# You have a website? Maybe you need some analytics!

When I created this website, I did not included any web statistics usages. I naively though I didn't care.

But, finally, it came the moment when I wondered: _did someone read my posts?_

I know for sure that I have been read, at least my [OPA post](/post/openpolicyagent-1) (👋 Hello [Anders Eknert](https://twitter.com/anderseknert) and all those retweeted!)... but that's all I know...

Definitely, I needed some basic stats!


**Here is a quick post about what I did for this site.**

## The classical approach: Google Analytics

Yep. I could have use the classical [Google Analytics](https://analytics.google.com/analytics/web/#/) approach: simple, easy to implement with the theme I choosed, free...

However, I wanted something more open source. Ideally, it would hbe free, easy to use and out of the scope of classical big tech giants.

> **Note**
>  
> There is a movement, in France, supported by an association to ["degooglize" the internet](https://degooglisons-internet.org/) (ie to stop internet centralization on GAFAM solutions, and to promote Open sources alternatives). I am not an activist (I use a lot of Google Services), but I'd like this idea!

That is why I did not select option.

## Alternative approach: GoatCounter 🐐

Yes! If you are reading this, you are considered as goats! :smile:

So, I just integrated the [GoatCounter](https://www.goatcounter.com/) free account (as I'm not doing commercial usage of it)!

I let you browse the website, as I cannot display something interesting yet (yes, it is in place since yesterday (October 14th), and I do not have so much traffic :sweat_smile:).

It is basic, but efficient (except ifg you have a [Brave browser](https://brave.com/)or an AdBlocker, but that's fine for me).

### Hugo integration

As my website is in [Hugo](https://gohugo.io), I had to adjust the template [I use and forked](https://github.com/arnaduga/github-style).

Quite easy: I just edited the `/layouts/partials/extended_heads.html` file by adding this code:
```html
{{ if .Site.Params.GoatCounter }}
<script data-goatcounter="https://{{ .Site.Params.GoatCounter }}.goatcounter.com/count" async  src="//gc.zgo.at/count.js"></script>
{{ end }}
```

... and adjusted my `config.yaml` file to have a 
```yaml
params:
  goatCounter: "arnaduga"
```

attribute.

**And that's all good!**

(I told you it was a short post :wink:)