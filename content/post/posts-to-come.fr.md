---
title: "Que le temps passe vite..."
date: 2022-07-25T19:00:00+02:00
summary: "Trop longtemps que rien n'a été publié ici... Essayons donc de faire revivre ce site 😅"
draft: false
pin: true
tags: ["francais", "general"]
translation: "posts-to-come"
---

Oh bon sang !

_Mais quelle honte..._

J'ai démarré ce _"blog"_ il y a presque un an, et ... cela fait plus de 7 mois qu'il n'a pas bougé.

Le 9 décembre 2021... La date de mon dernier article. Une éternité 😓

![image](../../media/memes/long-time-no-see.jpg)

Depuis cette date, je dois admettre qu'il y a eu quelques changements dans ma vie professionnelle.

J'ai démissionné de mon boulot et j'ai démarré une activité de consultant indépendant dans mon domaine: **architecte cloud, solution et API**.

J'ai déjà eu l'occasion d'exercer en tant qu'indépendant, j'avais commencé juste avant le COVID, mais j'ai eu besoin de retourner à mon boulot d'avant, comme salarié dans une industrie.

Bref. Pour de multiples raisons que je n'évoquerai pas ici, je suis donc revenu chez mon employeur, pour démissionner à nouveau... 😁

J'ai rapidement trouvé une mission (grâce à un de mes amis) pour intégrer un programme important de _digitalisation_, avec le rôle d'_API Tech Lead_.

**Ma mission** (écrite sur un document qui ne s'est _pas_ autodétruit) : assister mon client dans sa transformation digitale et plus particulièrement concernant les _APIzation_ : définir les API représentant les entités métier, la gouvernance et en implémentant à la fois l'infrastructure et les premiers proxys API.

Et comme corollaire : former les équipes opérationnelles et de développement.

Pourquoi est-ce que je vous raconte tout ça ? C'est à cause du contexte **technique**, bien sûr ! 😁

Voici quelques un des sujets qui pourraient donner lieu à un article sous peu :

- Le **_classique_** standard Open API Specifications 📜 , le phare dans la tempête,
- Le **_robuste_** [**Google APIGEE X**](https://cloud.google.com/apigee?hl=fr) 💱 , .... ,
- Le **_bien-aimé_** [**Postman**](https://www.postman.com/) 🛠️ , encore plus équipé qu'un couteau suisse
- Le **_damné_** Portail Developpeur 🧭 , toujours perdu au fin fond du backlog,
- Les **_magiques_** API 🪄 , _"connues"_ de tous mais très souvent maltraitées ou dévoyées,
- L' **_envoùtant_** shell 🧜‍♀️ , duquel je suis tombé amoureux...

Alors, oui, je tente rescuciter ce blog pour vous partager un peu de tout ça, et, espérons-le, sur une base régulière (et pas tous les 7 mois 🤞🏼) 😉

![image](../../media/memes/see-you-soon.jpeg)

> _**Note**_: ce n'est probablement pas la première fois (ni la dernière) que je l'écris mais ce blog n'est _**PAS**_ sponsorisé. Je ne gagne RIEN des propriétaires des marques que je cite (ni de personne d'autres d'ailleurs).
