---
title: "Extended Readme"
date: 2021-08-31T00:00:00+02:00
draft: false
pin: false
tags: ["english"]
summary: "About me and this blog, some information."
translation: "extended-readme.fr"
---



# About

Yes, I know, you may wondering why I'm starting now a website, blog-style, in 2021. 

I can hear you from there: *"you should use Twitter, Facebook, Medium or other kind of media!"*.

Yes, I could. But, I wanted to tested something else, something new (to me): [HUGO](https://gohugo/io) static web site generator.

But, before going further onto this topic, let me introduce myself.


## Who am I?

I'm a French **solution and API architect**, 40+ years old, working in IT since *the 20th century* :smile:

I work on several kinds of topics: Cloud Transformation programs, DevOps team coaching, support to development team in software design (not UI), API interface contratct definition, API envangelisation, etc.

I'm very curious and quite opened-mind by nature: I love to discover new things, new tools, new principles, new concepts (even if I have to admit, I'm lost in _blockchains_ and _cryptocurrencies_ world).

> **Note**
> 
> I'm **NOT** a developer. 
> 
> I use to code for personal usages, for Arduino or Raspberry Pi, or a little bit of NodeJS, Docker, etc... But **NOT** in a professional way. There are so many good coders that do it much better than me!
>
> However, I love to code. I love to work with a development team, to help them designing the application.


## Why this blog?

Well, let's say I wanted to write down some notes, some ideas and some discovers I could make. And also to have a kind of "memo" of my investigations. 

Twitter account is too limited to efficiently share this (and Yes, Ranjit, I can be chatty, I know!)...  I didn't need more arguments to start this! :smile:


## Why in English 🇬🇧, if I'm French 🇫🇷?

Well, I know I do a lot of mistakes and typo in English. But, actuyally, I always worked for big companies with teams all over the globe: very often in India 🇮🇳, but also in Thailand 🇹🇭 or Romania 🇷🇴. So it looked natural to me to use this language for a more technical blog. 

In addition to that, we have to admit the litterature (on Internet) about technical topics is mainly in English!

Nevertheless, it might be some French posts as well, don't worry! :smile:


## Notes about the website

The website is build using [HUGO](https://gohugo.io/) and [github-style theme forked and adjusted with my own features](https://github.com/arnaduga/github-style).

Dates of the content and posts _could_ be altered: I have tons of unpublished notes taken long time ago that could make good content. I would try to place them correctly on a timeline.

## Shared links

Links mentioned on this site (to Amazon, Gandi, ALI or others) **ARE NOT** sponsorised.

I will NOT earn anything if you click on them! :smile: