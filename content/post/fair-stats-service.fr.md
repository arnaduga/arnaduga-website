---
title: "Statistiques de traffic web"
date: 2021-10-15T22:36:45+02:00
summary: "Analyser le traffic de votre site web sans utiliser Google Analytics: GoatCounter !"
pin: false
draft: false
tags: ["francais","outils"]
translation: "fair-stats-service"
---

# Besoin de statistiques pour votre site web ?

Quand j'ai créé ce site web, je n'avais pas prévu d'y ajouter un outil d'analyse de traffic. Je pensais naïvement que je m'en fichais. 

Mais finalement, j'en suis venu quand même à me dire que, même si je fais ce site surtout pour moi, je suis curieux de savoir s'il est vu par quelqu'un d'autres :smile:.

Je suis certain d'avoir été lu, notamment sur mon seul article un peu intéressant à ce jour sur [OPA (en anglais seulement)](/post/openpolicyagent-1) (👋 Hello [Anders Eknert](https://twitter.com/anderseknert) et tous ceux qui l'ont retweeté !)... mais j'en sais pas beaucoup plus...

Oui, c'est certain, j'ai besoin de quelques statistiques basiques !

**Voici un article rapide sur ce que j'ai donc fait pour ce site.**


## L'approche classique : Google Analytics

Oui. J'aurai pu utiliser le très classique [Google Analytics](https://analytics.google.com/analytics/web/#/) : simple, facile à mettre en place, notamment dans le thème Hugo choisi pour ce site, gratuit ...

Pourtant, je voulais quelque chose d'autre. Quelque chose d'Open Source. Idéalement, gratuit, facile d'utilisation, sans nécessité d'auto-hébergement, et pas dans le giron des géants de la tech.

> **Note**:
> 
>  Je ne sais pas si vous connaissez le mouvement ["degooglisons" internet](https://degooglisons-internet.org/) (ie arrêtons d'utiliser les services gratuits et centralisés des GAFAM pas toujours hyper respectueuses de nos données, notamment au profit de solution Open Source). Je suis loin d'être un activiste (je suis utilisateurs de nombreux services Google), mais j'aime beaucoup les idées qu'ils portent !

C'est pour cela que je n'ai pas choisi cette option.

## Approche alternative : GoatCounter 🐐

Oui ! Si vous êtes en train de lire cet article, c'est que vous êtes maintenant considérés comme des chèvres ! :smile:

Donc, voilà; j'ai juste intégré un compteur [GoatCounter](https://www.goatcounter.com/), avec un compte gratuit car je n'en fais pas d'usage commercial.

Je vous laisse éplucher leur site et leurs captures d'écran. En effet, pour le moment mon compte ne m'affiche pas grand chose : je l'ai mis en place hier seulement, et comme je ne génère que peu de traffic... il n'y a pas encore d'effets visibles :sweat_smile:.

C'est plutôt basic et rudimentaire, mais c'est efficace.... sauf si vous utilisez le navigateur [Brave](https://brave.com/) ou un AdBlocker qui bloquera le script et donc, ne vous comptabilisera pas... mais pour ce que je cherchais à faire, c'est pas gênant.

### Integration avec mon thème Hugo

Comme mon site est généré à l'aide d'[Hugo](https://gohugo.io), j'ai dû ajuster [le modèle que j'ai utilisé, après l'avoir _forké_](https://github.com/arnaduga/github-style).

Plutôt simple. J'ai modifié le fichier `/layouts/partials/extended_heads.html` en y ajoutant ces lignes de code :
```html
{{ if .Site.Params.GoatCounter }}
<script data-goatcounter="https://{{ .Site.Params.GoatCounter }}.goatcounter.com/count" async  src="//gc.zgo.at/count.js"></script>
{{ end }}
```

... et en ajustant en conséquence mon fichier de configuraiton `config.yaml` : 
```yaml
params:
  goatCounter: "arnaduga"
```

**Et c'est parti !**

(je vous avais dit que ce serait un article rapide :wink:)