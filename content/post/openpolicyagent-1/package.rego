package arnaduga.api.authz

default allow = false


# Prepare the path
pathExploded := split(input.request.path,"/")




# Determine is the caller is the owner
is_owner {
	input.token.username == pathExploded[1]
}

# Function to identify legitimate callers
is_legitimate { is_owner }
is_legitimate { 
	some i
    	input.token.roles[i] == "keymanager"
}


# Allow create key IF the user is owner
allow {
	is_owner
    input.request.method == "POST"
}

# Allow an authenticated user to get someone public key
allow {
	input.token.username
    {pathExploded[2], pathExploded[3]}  == {"keys","public"}
    input.request.method == "GET"
}


# Allow only owner to get its own private key
allow {
	is_owner
    {pathExploded[2], pathExploded[3]}  == {"keys","private"}
    input.request.method == "GET"
}


# Allow legitimate user and keyadmin to PUT/DELETE a keypair
editMethods { input.request.method == "PUT"}
editMethods { input.request.method == "DELETE"}


allow {
	is_legitimate
	editMethods
    pathExploded[2] == "keys"
    not pathExploded[3]
}


# Allow if the call is GET /healthcheck, whatever the user
allow {
	input.request.path == "/healthcheck"
    input.request.method == "GET"
}
