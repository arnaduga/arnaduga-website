---
title: "Google Apigee X - Part 1"
date: 2022-08-05T19:00:00+02:00
summary: "Google Apigee X architecture considerations"
draft: false
pin: true
tags: ["english", "cloud", "architecture"]
---

# Google Apigee X

This post will NOT describe what is Google Apigee from a functional point of view, as there are plenty of resources on the net (see [Further Readings](#further-readings) section) : Apigee is an API Management layer, that sits between your consumers/clients/callers/yourself and the backend.

> **Note**: this post is **NOT** sponsorised by Google or any other companies. It is only the result of my personal experience acquired during missions using this tools

## Apigee Edge vs Apigee X?

### Early days ...

Apigee solution has been bought by Google in 2016. In 2017/2018, when I started to work on it, it was sold as a SaaS solution named **Google Apigee Edge**.

At this time, the solution was not integrated to Google Cloud Platform at all. As a surprising consequence, when you bought a licence, the Google technical team offered the possibility to instanciate Apigee Edge on GCP (of course) or _AWS_ 🤨 !

It could be convenient if you wanted to optimize network traffic by "using" AWS region where GCP was not present.

> Whatever your choice, GCP or AWS, you DID NOT get any access to the account hosting your instance: it was fully managed by Google!

### ... and nowadays

Since ~2022 (I do not have the exact date), Google integrated Apigee into the **Google Cloud offers**, with the **Apigee X** solution, that can be found in the Google Cloud console:

![image](googlecloud-1.png)

And then, now, no more Google Cloud or AWS hosting choice to make: it is obviously hosted on Google Cloud `\o/`.

As any other Google Cloud service, you would be able to create instanciation region, multi-region replication if needed, etc.

But, as other services, it is not just an option to activate: you will have to smart instanciate multiple Apigee Organizations in different region, and set a Global Load Balancer, cleverly. Not always a piece of cake...

> Note: the integration to Google Cloud is quite recent. It is not completely finalized, and then, there is still some integrations issues. But it getting better and easier releases after releases.
>
> _To mention one_: you need to instanciate a **MIG** -Managed Instance Group, in order to routes requests. MIG is composed of 2 VM... you have to manage and renew to patch...
>
> Honnest additional mention: the MIG SHOULD be replaced by a [Google PSC -Private Service Connect](https://cloud.google.com/apigee/docs/api-platform/system-administration/psc-overview), an equivalent VPC Endpoint. As of now, this option is still in **Pre- General Availability**, and then, not recommended to be used in production. Soon...

## Global architecture

Apigee is a "middleware" that exposes API for you, let you define request/response treaments like mediation, security and other and then, forward the request to backends.

The APIGEE instance is obviously composed of multiple elements: UI, management server, Unified Analytics Platform, Connector, MART, Message processor, Cassandra DB, watcher, UDCA etc... (non exhaustive list as it is not important at this stage). All of them are organized in TWO main category: **management plane** and **resource plane**.

Both of them are managed by Google (in the SaaS version, not the hybrid's one), and intensively relies on a Kubernetes cluster - GKE or Google Kubernetes Engine. Surprising, isn't it? :wink:

Here a diagram to better understand (_one schema is worth a thousands words_):

![image](apigee-x-1.png)

### Google Cloud Project vs Google Apigee Organization

Wording is often source of misunderstanding. ANd this merge between Apigee and Google Cloud created also some weird discussion in the team...

Let me try to summarize:

|                      | Google Cloud Context                                             | Google Apigee X context                              |
| -------------------- | ---------------------------------------------------------------- | ---------------------------------------------------- |
| Word **Project**     | A set of resources (eq of Azure Resource Groups)                 | Nothing in particular                                |
| Word **Orgnization** | Accounts relationship at the company scale                       | An instance of Apigee X, with multiple environments  |
| Word **Environment** | You generally have multiple accoutns for multiples envfironments | An object part of organization (a deployment target) |

So, please, when talking with your team, say "Google Orgnization" or "Apigee Organization" to avoid any confusion (I went through it...).

In addition to that, the **_Google Cloud Project ID_** MUST be the same as the **_Google Apigee X Organization_**. Great! :smile:

> Keep that in mind, I'll mention it later in the provisioning section.

### Network

In the documentation, you will see several networks and subnets are mentioned:

- a network with CIDR size of **/22**, for the core part
- a network with CIDR length of **/28**, for Google Support
- another subnet for routing MIG (managed instance group)

![meme](../../media/memes/networking.jpg)

We found the doc a little bit confusing: the CIDR /22 and /28 are NOT network to be defined on _your_ Google account, but will be **reserved** only for the Google usage, on the otherside of the peering.

> _This means, you don't need to create a subnet of size /22, /24 or /28 size!_.
>
> Just to _block_ them, to be sure you will never ever overlap these ips with Google Apigee service in your Google Project when you will create future subnets.

**So, in that case, do I need to set a subnet on my own network?**

![that-s-a-good-question](../../media/memes/thats-a-good-question.jpg)

Well ... you don't need to have a lot of resources on your own network/VPC, except the mentioned-above MIG and LB. That is the third subnet mentioned!

So, you _have_ to create a subnet, but if you want to do it compliant with your own network addresses plan, no need to be too generous.

In the team I work for, the network was sized **/25**. Seems good as far as I know 👍🏻

## What's next?

**Next step is a jump to the provisoning journey!**

But ... that's enough for this post. Next one to come soon :smile:

![meme](../../media/memes/tobecontinued-1.jpg)

## Further readings

- [Google APIGEE](https://cloud.google.com/apigee/) website for a generic reading
- [Google Blog Post](https://www.googlecloudcommunity.com/gc/Cloud-Product-Articles/Network-and-Envoy-Proxy-Configuration-to-manage-mTLS-on-Apigee-X/ta-p/175146) by Joël Gauci about network and other Apigee X instanciation (Envoy)
- Another [Google blog post by Joel Gauci](https://www.googlecloudcommunity.com/gc/Cloud-Product-Articles/Network-and-Routing-Configuration-to-manage-APIs-hosted-on-prem/ta-p/173374), co-authored with Christophe Lalevee and Ibrahima N'doye, about Network and routing for on prem install (but there are some common part with SaaS version)
