---
title: "Long time no see..."
date: 2022-07-25T19:00:00+02:00
summary: "Too long time without any post... Trying to re-launch the site 😅"
draft: false
pin: true
tags: ["english", "general"]
translation: "posts-to-come.fr"
---

Oh my god!

_What a shame..._

I started this _"blog"_ almost one year ago, and... it stayed unchanged for 7 months already!

December, 9th 2021... My last post date... An eternity 😓

![image](../../media/memes/long-time-no-see.jpg)

Since this date, I have to admit there were quite a few changes in my professional life.

I quit my job and I started a freelance activity, in my domain: **cloud, api and solution architect**. I already worked in such position, just before the COVID, but I needed to come back to my previous position as employee in an industry.

TL;DR: for many reasons my friends already know, I came back and quit ... again 😁

I quickly found a mission (thanks to a friend of mine) in a company to integrate a massive digitalisation program, with the specific role of _API tech lead_.

**My mission**: supporting the IT transformations on the _APIzation_ domain by helping them defining APIs representing the business entities, governance and implementing both infra and first API proxy.

As a corollary, training operation team and developers.

Why am I mentioning that? Because of the **technical** context, of course! 😁

Here are the fex element I may use as post idea soon:

- The _classical_ standard Open API Specifications 📜 , the lighthouse in the storm,
- The _robust_ [**Google APIGEE X**](https://cloud.google.com/apigee) 💱 , a great abstraction layer,
- The _beloved_ [**Postman**](https://www.postman.com/) 🛠️ , more equipped than a swiss knife
- The _forgotten_ Developer Portal 🧭 , always at the bottom of the backlog,
- The _magical_ API 🪄 , _"known"_ by everyone but often mistreated and deviated
- The _mesmerizing_ shell 🧜‍♀️ , I fall in love with...

So yes, I'm resurrecting this blog to share a little about all of this, hopefully on a regular basis (not once every 7 months) 😉

![image](../../media/memes/see-you-soon.jpeg)

> _**Note**_: probably not the first time I write it (and not the last one), but this blog is _**NOT**_ sponsorised in any way. I do not get paid by any brand owner I mention in my posts (or from anyone else).
