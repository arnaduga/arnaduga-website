# Hello World! 🇬🇧 

This is just _my_ personal tech blog.

Posts will be in French and English: I'll keep a pinned post to be used as table of content.

> [About me or this blog](post/extended-readme/)
        
---

# Bonjour le monde ! 🇫🇷

Ceci est juste un petit blog tech personnel, sans prétentions.

Les articles seront en français et en anglais (le plus souvent, je pense): je maintiendrai un article épinglé spécialement pour jouer le rôle de table de matière.

> [À propos de moi ou de ce blog](post/extended-readme.fr/)

