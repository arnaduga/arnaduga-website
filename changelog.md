# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> Note: the posts changes (add a post, fix typo in post, etc..) won't be logged here. Just the change in "backend", site setup, etc.


## 2022-07-22

### Changed
- Updated the command line for local display

## 2021-09-16
### Added
- Initial version

### Changed
- converted `config.toml` to `config.yaml`
- migrated from `github-style` to `arnaduga-github-style` in order to maintain updates
- Default image, by an icon made by Freepik from www.flaticon.com
- Change logo: icons made by Payungkead from www.flaticon.com