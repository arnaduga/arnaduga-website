# What's that?

This repository, and the associated Gitlab Pages, are the sources of my personal blog: [`https://arnaduga.dev`](https://arnaduga.dev).


# Contributing

Just in case you are interested in participating, by suggesting a post, asking question or suggest a typo fix (yeah, I know, I do a lot of typos... ), here is the process.

## New post idea

As it is a personal blog, I prefer the content to by _mine_, in the sense, written with my words. So, for post ideas an suggestions, please use:
- Gitlab issues tracking: [create a new issue](https://gitlab.com/arnaduga/arnaduga-website/-/issues/new?issue) labelized `post::idea` (or I'll labelized it myself, don't worry)
- ... or send an [email](mailto:contact@arnaduga.dev)

# Few notes

## Local server

As soon as a post is `Draft: true`, it won't be considered.

During writing moment, the command to launch a local server that consider also drafted posts is: `hugo server -D --config config-dev.yaml --watch --buildFuture`

THe `config-dev.yaml` file is just a way to prevent loading the GoatCounter stats.

## Content management / Authoring

### Smileys

Thanks to the Hugo option `enableEmoji: true`, it is possible to use smiley conversion by using keywords, like `:smile:` or `:house:`.

You can refer to the [emoji cheat sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) for list of keywords.